
let min = +prompt('please enter first number', '');
let max = +prompt('please enter second number', '');

while(isNaN(min) || !min){
    min = +prompt('please enter again', '');
}

while(isNaN(max) || !max){
    max = +prompt('please enter again', '');
}

while (min > max){
    min = +prompt('enter again', '');
}

for(let i = min; i <= max; i++){
    if (i % 2 === 0){
        console.log(i);
    }else{
        console.log('sorry no number');
    }
}
